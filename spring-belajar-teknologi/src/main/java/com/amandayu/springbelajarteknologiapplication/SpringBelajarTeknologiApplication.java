package com.amandayu.springbelajarteknologiapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBelajarTeknologiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBelajarTeknologiApplication.class, args);
	}

}
